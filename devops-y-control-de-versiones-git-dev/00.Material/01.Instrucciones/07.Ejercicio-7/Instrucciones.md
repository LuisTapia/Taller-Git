# Ejercicio 7
Comandos que utilizaremos: [Manual](Manual.md)

## Rebase y rebase interactivo 
Para este ejercicio utilizaremos GitHub Visualizing una herramienta web super interesante que nos permite ir creando el grafo de los commit que hagamos en la herramienta para ejemplificar de mejor manera como funciona git rebase.

* Ingresemos a [GitHub Visualizing](http://git-school.github.io/visualizing-git/)
* Ingresemos los siguientes comando en el siguiente orden:

```
git commit
git checkout -b topic
git checkout master
git commit
git commit
git checkout topic
git commit
git commit
git commit
git rebase master
```

* Podremos visualizar el uso de rebase donde sincroniza todos los cambios de una rama en otra. Esto permite en ocasiones una fusión de ramas más limpia.