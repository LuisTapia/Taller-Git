# Ejercicio 5
Comandos que utilizaremos: [Manual](Manual.md)

## La magia del stash
Imagina el siguiente escenario: estás programando un nuevo feature de un sistema que está planificado para dentro de 2 meses. Acabas de comenzar a programar, tienes muchas cosas adelantadas pero nada completamente que merezca ese primer commit o si simplemente necesitas pasarte a la rama del código productivo para revisar esa función que borraste y ahora pareciera ser necesaria. Para eso y mucha más se encuentra stash.

* Volvamos, si no es que ya estás allí, a la rama master

```
git checkout master
```

* Creamos otra rama a partir de master con el nombre pruebaStash

```
git checkout -b pruebaStash
```

* Vayamos a la clase SayayinDTO y hagamos cualquier tipo de modificación. En mi caso eliminaré la función "generarMensajePoder" y guardemos.
* En estos momento surge la necesidad de revisar el código productivo y modificarlo (master) porque un usuario nos notifica de un error en ambientes productivos.

Si realizamos simplemente un git checkout master estaremos llevandonos con nosotros todas las modificaciones hechas que no se le han hecho commit de la rama anterior. Es allí donde podemos hacer uso de stash

```
git stash
```

* Podemos ver la pila de stash haciendo uso del comando 

```
git stash list
```

* Ahora sí regresemos a la rama master y hagamos todas lo que necesitamos ya que nuestros cambios están a salvo en el stash
* Para volver a sacar las modificaciones realizadas del stash de vuelta a nuestra rama de trabajo puedes hacer uso de:

```
git stash pop
```

* Limpiemos la pila de stash

```
git stash clear
```
